import { Request, Response } from 'express';
import { RoutesInput } from '../types/route';
import { CreateDatapoint, ListDatapoints } from '../controllers/datapoint.controller';

/**
 *
 *
 *
 **/
export default ({ app } : RoutesInput) => {

  app.get('/', async (_req: Request, res: Response ) => {
    res.send('TS App is Running');
  });

  // Save a datapoint
  app.get('/api/datapoint/create', CreateDatapoint);
  app.get('/api/datapoint', ListDatapoints);

}
