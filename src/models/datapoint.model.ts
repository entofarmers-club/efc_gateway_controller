import {
  Entity,
  Column,
  PrimaryGeneratedColumn
} from 'typeorm';
import {
  Exclude,
  Expose,
} from 'class-transformer';
import {
  IsDate,
  IsInt,
  IsNumber,
  IsPositive
} from 'class-validator';


@Exclude()
export class NewDatapointDto {

  @Expose()
  @IsNumber()
  value: number

  @Expose()
  @IsDate()
  timestamp: Date

  @Expose()
  @IsInt()
  @IsPositive()
  stream_id: number
}

@Entity()
export class Datapoint {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  value: number;

  @Column({
    type: 'timestamptz',
    default: new Date()
  })
  timestamp: Date;

  @Column({
    nullable: true
  })
  stream_id: number;
}
