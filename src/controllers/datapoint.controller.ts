import { Request, Response, NextFunction } from 'express';
import { SaveDatapoint, FetchDatapoints } from './../services/datapoint.service';

/**
 *
 *
 *
 **/
const CreateDatapoint = async (req: Request, res: Response, _next: NextFunction) => {

  // const data = req.body;
  const data = {
    stream_id: 1,
    timestamp: new Date(),
    value: Math.floor((Math.random()*100 + 1))
  };

  const datapoint = await SaveDatapoint(data);

  return res.json(datapoint);
}

/**
 *
 *
 *
 **/
const ListDatapoints = async (_req: Request, res: Response, _next: NextFunction) => {
  const points = await FetchDatapoints();
  return res.json(points);
}

export {
  CreateDatapoint,
  ListDatapoints
};
