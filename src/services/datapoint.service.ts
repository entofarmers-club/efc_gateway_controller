import { getRepository} from 'typeorm';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import { Datapoint, NewDatapointDto } from './../models/datapoint.model';

/**
 *
 *
 *
 **/
async function SaveDatapoint(data: NewDatapointDto): Promise<Datapoint> {

  const newDpDto = plainToClass(NewDatapointDto, data);

  const errors = await validate(newDpDto);
  if (errors.length) {
    console.warn(errors);
    throw new Error(errors.toString());
  }

  getRepository(Datapoint).create(newDpDto);

  return getRepository(Datapoint).save(newDpDto)
}

/**
 *
 *
 *
 **/
async function FetchDatapoints(): Promise<Datapoint[]> {

  return getRepository(Datapoint).find();
}

export {
  SaveDatapoint,
  FetchDatapoints
};
