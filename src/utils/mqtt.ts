import * as mqtt from 'mqtt';

/**
 *
 *
 *
 **/
const client  = mqtt.connect('mqtt://oif-mqtt.urbanlink.nl:8020', {
  clientId: '123-456-789',
  username: '2222-222',
  password: '333'
});
console.log('1');

client.on('error', (err) => {
  console.log(err.toString());
})

client.on('connect', () => {
  console.log('connected');
  client.subscribe('presence', function (err) {
    if (!err) {
      client.publish('presence', 'Hello mqtt')
    }
  })
})

/**
 *
 *
 *
 **/
class MQTT {

  client: any;

  private url: string;
  private config: any = {
    clientId: '123-456-789',
    username: '2222-222',
    password: '333'
  }

  constructor() {

  }

  init(url: string, config: any) {
    this.url = url;
    this.config = config;
  }

  connect() {
    this.client = mqtt.connect(this.url, this.config)
  }

  private handleData() {

  }
}

export default MQTT;

// var Cayenne = require('cayennejs');
//
// // Initiate MQTT API
// const cayenneClient = new Cayenne.MQTT({
//   username: "71c37b60-fa6b-11e6-ac86-a9a56e70acce",
//   password: "075fac11b9c97547303a079de001e51e6b92b528",
//   clientId: "8091d4f0-e134-11e8-a056-c5cffe7f75f9"
// });

// client.on('message', function (topic, message) {
//   // message is Buffer
//   console.log(message.toString())
//   client.end()
// })
//
//
//
// cayenneClient.connect((err, mqttClient) => {
//   console.log(err, mqttClient);
//   start();
//
//   // dashboard widget automatically detects datatype & unit
//   cayenneClient.kelvinWrite(3, 65);
//   // sending raw values without datatypes
//   cayenneClient.rawWrite(4, 123);
//
//   // subscribe to data channel for actions (actuators)
//   cayenneClient.on("cmd9", function(data) {
//     console.log(data);
//   });
//
// });
//
// function start() {
//   setInterval(() => {
//     console.log('ping')
//     cayenneClient.celsiusWrite(0, Math.floor(Math.random() * Math.floor(100)) );
//   }, 1000);
// }
