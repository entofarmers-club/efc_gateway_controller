import * as rf from 'nrf24';

class NRF24 {

  pins = {
    CE: 22,
    CS: 0,
    IRQ: 27
  }
  radio: any;

  config = {
    PALevel: rf.RF24_PA_HIGH,
    DataRate: rf.RF24_250KBPS,
    CRCLength: rf.RF24_CRC_16,
    Channel: 76,
    AutoAck: true,
    retriesCount: 2,
    retriesDelay: 15,
    Irq: this.pins.IRQ
  };

  pipes = [ '0xABCDABCD71', '0x544d52687C' ];

  constructor() {
     console.log('nrf24 constructor');
  }

  init(config, pipes, pins): Boolean {

    console.log('nrf24 init');

    this.config = config;
    this.pipes = pipes;
    this.pins = pins;

    this.radio = new rf.nRF24(this.pins.CE, this.pins.CS);
    this.radio.begin(true);

    const ready = this.radio.present();
    if (!ready) {
      console.log("ERROR:Radio is not ready!");
      //process.exit(1);
      return false;
    }

    this.radio.config(config,true);

    this.receive();

    return true;
  }

  // Reveive
  receive() {
    const pipe = this.radio.addReadPipe(this.pipes[ 1]);
    this.radio.changeReadPipe(pipe, this.config.AutoAck, rf.RF24_MAX_MERGE); // Max merge pckts
    console.log("Stating Reading on pipe addr:", this.pipes[ 1], " #:", pipe);

    this.radio.read(function(d: any, items: number) {
      for(var i=0; i<items; i++) {
        console.log("Payload Rcv, replied:", d[ i].data.toString('utf8')); //radio.write(d[i].data));
      }
    },function() { console.log("STOP!"); })
  }

  // Send
  send(do_sync: boolean = false) {
    this.radio.useWritePipe(this.pipes[1]);
    this.radio.addReadPipe(this.pipes[0]);
    console.log("Open Write Pipe " + this.pipes[1]+ " reading from " + this.pipes[0]);

    let tmstp: number;
    let roundtrip: [number,number];
    let sender = function() {

      var data = Buffer.alloc(4);
      tmstp = Math.round(+new Date()/1000);
      data.writeUInt32LE(tmstp, 0);
      process.stdout.write("\nSending....");
      roundtrip = process.hrtime();

      if (do_sync) {
         if (this.radio.write(data)) process.stdout.write("[Sync] Sended " + tmstp);
         else process.stdout.write("[Sync] Failed to send " + tmstp +" ");
      } else {
        this.radio.write(data, function(success: Boolean) {
          if (success) process.stdout.write("[Async] Sended " + tmstp);
          else process.stdout.write("[Async] Failed to send " + tmstp +" ");
        });
      }
    };
    this.radio.read(function(d: any, n: number) {
      if (n==1) {
        let t = process.hrtime(roundtrip);
        process.stdout.write(" | response received |")
        let ret = d[0].data.readUInt32LE(0);
        if( ret == tmstp) process.stdout.write(" reponse matchs ");
        else process.stdout.write(" response does not match ");
        console.log("| roundtrip took", (t[1] /1000).toFixed(0)," us");
      }
    }, function() { console.log("STOPPED!"); });

    setInterval(sender,1000);
  }
}

export default NRF24;
