import 'reflect-metadata';
import dotenv from 'dotenv';
dotenv.config();
import express, { Application } from 'express';
import {createConnection} from 'typeorm';
import bodyParser from 'body-parser';
import Routes from './routes';
import { typeOrmConfig } from './config/ormConfig';

/**
 *
 *
 *
 **/
(async () => {
  console.log(Date());

  // database connection
  try {
    await createConnection(typeOrmConfig);
    console.log('Connection with database established.');
  } catch(err) {
    console.warn(err);
  }

  // Setup the app
  const app : Application = express();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  // Initiate routes
  Routes({ app })

  // Start webserver
  const PORT = process.env.PORT || 4000;
  app.listen(PORT,() => {
    console.log(`server is running on PORT ${PORT}`)
  });
})()
