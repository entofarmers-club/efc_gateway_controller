FROM node:12.13-alpine
MAINTAINER Arn van der Pluijm <arn@urbanlink.nl>

WORKDIR /app

#RUN \
#  apt-get install libncurses5-dev && \
#  mkdir ./rf24libs && \
#  cd ./rf24libs && \
#  git clone https://github.com/tmrh20/RF24.git RF24 && \
#  cd RF24 && \
#  make install -B

COPY package.json ./
RUN npm install --quiet

COPY . .

#RUN ["npm", "run", "dev"]
