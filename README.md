# The Entofarmers Club Gateway

## About
The Farm gateway controlling all data and the communication with the backend server based on MQTT.

## Server
NodeJS based server.

### Modules used
[https://github.com/ludiazv/node-nrf24](https://github.com/ludiazv/node-nrf24)

## Hardware
Raspberry Pi 3b with a custom HAT. 
